using LINQtoCSV;
using System;
using System.Collections.Generic;
using System.IO;

namespace AutoTuner
{
	class Program {
		static Dictionary<string, TuningTable> TuningTableDict { get; set; }

		static List<string> ListOfTableNames = new List<string>() {
			TableName.IgnitionLowSpeed.ToString(),
			TableName.IgnitionHighSpeed.ToString(),
			TableName.FuelLowSpeed.ToString(),
			TableName.FuelHighSpeed.ToString(),
			TableName.CamAngleLowSpeed.ToString(),
			TableName.CamAngleHighSpeed.ToString(),
			TableName.WotCompLowSpeed.ToString(),
			TableName.WotCompHighSpeed.ToString(),
			TableName.WotCompCatProtectLowSpeed.ToString(),
			TableName.WotCompCatProtectHighSpeed.ToString(),
			TableName.KnockIgnitionLimitLowSpeed.ToString(),
			TableName.KnockIgnitionLimitHighSpeed.ToString(),
			TableName.KnockSensitivityLowSpeed.ToString(),
			TableName.KnockSensitivityHighSpeed.ToString(),
			TableName.KnockRetardLowSpeed.ToString(),
			TableName.KnockRetardHighSpeed.ToString(),
			TableName.TargetLambdaLowSpeed.ToString(),
			TableName.TargetLambdaHighSpeed.ToString(),
		};

		enum TableName {
			IgnitionLowSpeed,
			IgnitionHighSpeed,
			FuelLowSpeed,
			FuelHighSpeed,
			CamAngleLowSpeed,
			CamAngleHighSpeed,
			WotCompLowSpeed,
			WotCompHighSpeed,
			WotCompCatProtectLowSpeed,
			WotCompCatProtectHighSpeed,
			KnockIgnitionLimitLowSpeed,
			KnockIgnitionLimitHighSpeed,
			KnockSensitivityLowSpeed,
			KnockSensitivityHighSpeed,
			KnockRetardLowSpeed,
			KnockRetardHighSpeed,
			TargetLambdaLowSpeed,
			TargetLambdaHighSpeed,
		}

		static void Main(string[] args) {
			TuningTableDict = new Dictionary<string, TuningTable>();

			string pathToFile = "/Users/hector/Desktop/HONDATA/convertcsv.csv";
			CsvFileDescription inputFileDescription = new CsvFileDescription {
				SeparatorChar = ',',
				FirstLineHasColumnNames = true
			};
			CsvContext csvContext = new CsvContext();

			IEnumerable<DataLog> dataLogs =
				csvContext.Read<DataLog>(pathToFile, inputFileDescription);

			//string pathToKalFile = "/Users/hector/Desktop/HONDATA/10-9-18_BasedOnDrivingWithVTECSet7000.kal";
			string pathToKalFile = @"C:\Active\auto\AutoTuner\10-9-18_BasedOnDrivingWithVTECSet7000.kal";
			DeserializeFile.ReadInFile(pathToKalFile);
			List<TuningTable> tempTuningTable = DeserializeFile.CreateListOfTuningTable();

			tempTuningTable.ForEach(obj => PopulateTuningTableDictionary(obj));
		}

		static void PopulateTuningTableDictionary(TuningTable currentTuningTable) {
			TuningTableDict.Add(ListOfTableNames.Find((x) => x.Equals(currentTuningTable.Name)), currentTuningTable);
		}

		bool IsReadFileSuccessful(string pathToFileToUse) {

			return false;
		}
	}
}
