﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using static AutoTuner.Utility;
using System.Collections;

namespace AutoTuner {
	public class TuningTable {
		const string CAM = "Cam";
		const string ROW = "Row";
		public const int NumOfLines = 129;
		public const int NumOfLinesForSingleCamAngle = 29;

		[IsSerializable(true)]
		public int TableNumber { get; set; }
		[IsSerializable(true)]
		public string Name { get; set; }
		[IsSerializable(true)]
		public int Rows { get; set; }
		[IsSerializable(true)]
		public int Cols { get; set; }
		[IsSerializable(true)]
		public int NumCamTables { get; set; }
		[IsSerializable(true)]
		public int HighSpeed { get; set; }
		[IsSerializable(true)]
		public List<int> LiveTuningEnabled { get; set; }
		[IsSerializable(true)]
		public List<int> Rpm { get; set; }
		[IsSerializable(true)]
		public List<double> Load { get; set; }

		public double[,,] TableOfValues { get; set; }

		private object this[string propertyName] {
			get { return this.GetType().GetProperty(propertyName).GetValue(this, null); }
			set { this.GetType().GetProperty(propertyName).SetValue(this, value, null); }
		}
		public TuningTable() { }

		public void ExtractValue(List<string> itemToExtract) {
			//TODO: Convert to instead iterating through the itemToExtract
			TableNumber = Convert.ToInt32(itemToExtract[0].Split("e")[1].Split("]")[0]);

			var properties = typeof(TuningTable).GetProperties();

			foreach (PropertyInfo property in properties) {
				if (NumCamTables > 0) {
					if (NumCamTables == 1) {
						if (itemToExtract.Count() == NumOfLines) {
							itemToExtract.RemoveRange(NumOfLinesForSingleCamAngle, NumOfLines - (NumOfLinesForSingleCamAngle));
						}
					}
				}

				var temp = itemToExtract.Find(x => x.Contains(property.Name) /*|| x.StartsWith(CAM, StringComparison.CurrentCulture)*/);

				if (temp != null) {
					var currentValue = temp.Split('=')[1];
					if (temp.StartsWith("Table", StringComparison.CurrentCultureIgnoreCase))
						continue;
					
					if (CheckIfList(currentValue)) {
						if (CheckIfCamValue(temp.Split('=')[0])) {
							StoreIntoTableOfValues(temp.Split('=')[0], currentValue);
						} else {
							StoreValueIntoListProperty(property, currentValue);
						}
					} else {
						if (temp.StartsWith(nameof(LiveTuningEnabled))) {
							StoreValueIntoListProperty(property, currentValue);
						}
						else {
							dynamic mySecondTemp = Convert.ChangeType(currentValue, property.PropertyType);
							property.SetValue(this, mySecondTemp);
						}
					}
				} else {
					var myTemp = itemToExtract.FindAll(x => x.StartsWith(CAM, StringComparison.CurrentCulture));

					foreach (var item in myTemp) {
						string currentValue = item.Split('=')[1];
						StoreIntoTableOfValues(item.Split('=')[0], currentValue);
					}
				}
			}
		}

		private void StoreValueIntoListProperty(PropertyInfo property, string currentValue) {
			// Set the value of the current classes static list to a new instance of  which is of same type as the current class
			var tempList = Activator.CreateInstance(typeof(List<>).MakeGenericType(property.PropertyType.GenericTypeArguments.First()));
			List<string> tempString = ConvertCsvToList(currentValue);
			for (int i = 0; i < tempString.Count(); i++) {
				// Using the IList, add in all string values to the temporary list for storage into the actual property later
				((IList)tempList).Add(Convert.ChangeType(tempString[i], property.PropertyType.GenericTypeArguments.First()));
			}
			property.SetValue(this, tempList);
		}

		private bool CheckIfCamValue(string valueToCheck) {
			return valueToCheck.StartsWith(CAM, StringComparison.CurrentCulture);
		}

		private void StoreIntoTableOfValues(string tableIndexToExtract, dynamic listWithValues) {
			List<int> indicesToUse = ExtractIndices(tableIndexToExtract);

			List<string> listWithSeparatedValues = ConvertCsvToList(listWithValues);

			if (TableOfValues == null && Cols > 0 && Rows > 0)
				TableOfValues = new double[Cols, Rows, NumCamTables];
			if (TableOfValues != null) {
				for (int i = 0; i < listWithSeparatedValues.Count(); i++) {
					TableOfValues[i, indicesToUse[1], indicesToUse[0]] = (double)Convert.ChangeType(listWithSeparatedValues[i], TableOfValues.GetType().GetElementType());
				}
			}
		}


		//private int ExtractTableHieghtValue(string tableIndexToExtract) {
		//	var temp = tableIndexToExtract.Split("Row").First();
		//	var secondTemp = Convert.ToInt32(temp.ElementAt(CAM.Length));

		//	return secondTemp;
		//}

		private List<int> ExtractIndices(string valueToExtractFrom) {
			List<int> indicesToReturn = new List<int>(2);
			var temp = valueToExtractFrom.Substring(CAM.Length, 1);
			indicesToReturn.Add(Convert.ToInt32(temp));

			temp = valueToExtractFrom.Substring(CAM.Length + ROW.Length + 1, (valueToExtractFrom.Length - (CAM.Length + ROW.Length + 1)));
			indicesToReturn.Add(Convert.ToInt32(temp));

			return indicesToReturn;
		}

		private bool CheckIfList(string itemToCheckIfCommaSeparatedList) {
			return itemToCheckIfCommaSeparatedList.Contains(',');
		}
	}
}