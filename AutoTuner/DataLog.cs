﻿using System;
using LINQtoCSV;

namespace AutoTuner {
	public class DataLog {
		[CsvColumn(FieldIndex = 1)]
		public int frame { get; set; }
		[CsvColumn(FieldIndex = 2)]
		public double time { get; set; }
		[CsvColumn(FieldIndex = 3)]
		public int rpm { get; set; }
		[CsvColumn(FieldIndex = 4)]
		public double speed { get; set; }
		[CsvColumn(FieldIndex = 5)]
		public double map { get; set; }
		[CsvColumn(FieldIndex = 6)]
		public double clv { get; set; }
		[CsvColumn(FieldIndex = 7)]
		public double tps { get; set; }
		[CsvColumn(FieldIndex = 8)]
		public double camangle { get; set; }
		[CsvColumn(FieldIndex = 9)]
		public double targetcamangle { get; set; }
		[CsvColumn(FieldIndex = 10)]
		public double injector { get; set; }
		[CsvColumn(FieldIndex = 11)]
		public double ignition { get; set; }
		[CsvColumn(FieldIndex = 12)]
		public double ta { get; set; }
		[CsvColumn(FieldIndex = 13)]
		public double tw { get; set; }
		[CsvColumn(FieldIndex = 14)]
		public double lambda { get; set; }
		[CsvColumn(FieldIndex = 15)]
		public double targetlambda { get; set; }
		[CsvColumn(FieldIndex = 16)]
		public double shortterm { get; set; }
		[CsvColumn(FieldIndex = 17)]
		public double longterm { get; set; }
		[CsvColumn(FieldIndex = 18)]
		public double knockcount { get; set; }
		[CsvColumn(FieldIndex = 19)]
		public double voltage { get; set; }
		[CsvColumn(Name = "eld.v", FieldIndex = 20)]
		public double eldv { get; set; }
		[CsvColumn(FieldIndex = 21)]
		public double eld { get; set; }
		[CsvColumn(FieldIndex = 22)]
		public double s02 { get; set; }
		[CsvColumn(FieldIndex = 23)]
		public double brake { get; set; }
		[CsvColumn(FieldIndex = 24)]
		public double gear { get; set; }
		[CsvColumn(FieldIndex = 25)]
		public double boostduty { get; set; }
		[CsvColumn(FieldIndex = 26)]
		public double analog0 { get; set; }
		[CsvColumn(FieldIndex = 27)]
		public double analog1 { get; set; }
		[CsvColumn(FieldIndex = 28)]
		public double analog2 { get; set; }
		[CsvColumn(FieldIndex = 29)]
		public double analog3 { get; set; }
		[CsvColumn(FieldIndex = 30)]
		public double analog4 { get; set; }
		[CsvColumn(FieldIndex = 31)]
		public double analog5 { get; set; }
		[CsvColumn(FieldIndex = 32)]
		public double analog6 { get; set; }
		[CsvColumn(FieldIndex = 33)]
		public double analog7 { get; set; }
		[CsvColumn(FieldIndex = 34)]
		public double digital0 { get; set; }
		[CsvColumn(FieldIndex = 35)]
		public double digital1 { get; set; }
		[CsvColumn(FieldIndex = 36)]
		public double digital2 { get; set; }
		[CsvColumn(FieldIndex = 37)]
		public double digital3 { get; set; }
		[CsvColumn(FieldIndex = 38)]
		public double digital4 { get; set; }
		[CsvColumn(FieldIndex = 39)]
		public double digital5 { get; set; }
		[CsvColumn(FieldIndex = 40)]
		public double digital6 { get; set; }
		[CsvColumn(FieldIndex = 41)]
		public double digital7 { get; set; }


		public DataLog() { }
	}
}
