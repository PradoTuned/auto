﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AutoTuner {
	public class DeserializeFile {
		public static Dictionary<dynamic, List<string>> ObjectsForParsingAndTextFile = new Dictionary<dynamic, List<string>>();

		static List<string> linesOfFileToParse = new List<string>();

		public static void ReadInFile(string fileToReadIn) {
			string line = string.Empty;

			if (File.Exists(fileToReadIn)) {
				// Read the file and display it line by line.
				System.IO.StreamReader file = new System.IO.StreamReader(fileToReadIn);
				while ((line = file.ReadLine()) != null) {
					linesOfFileToParse.Add(line);
				}
				file.Close();
			}
		}

		public static List<TuningTable> CreateListOfTuningTable() {
			List<TuningTable> ListOfTuningTablesToReturn = new List<TuningTable>();

			for (int i = 0; i < 18; i++) {
				TuningTable tuningTable = new TuningTable();

				var indexOfTable = linesOfFileToParse.IndexOf("[Table" + i.ToString() + "]");

				var stringLinesForCurrentTable = linesOfFileToParse.GetRange(indexOfTable, TuningTable.NumOfLines);

				tuningTable.ExtractValue(new List<string>(stringLinesForCurrentTable));

				ListOfTuningTablesToReturn.Add(tuningTable);
			}

			return ListOfTuningTablesToReturn;
		}
	}
}