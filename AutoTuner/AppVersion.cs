﻿using System;
using System.Collections.Generic;

namespace AutoTuner {
	public class AppVersion {
		public const string NameTag = "[Application}";
		public static int NumOfLinesForAppVersion = 3;
		public string Version { get; set; }
		public string SaveDate { get; set; }

		public object this[string propertyName] {
			get { return this.GetType().GetProperty(propertyName).GetValue(this, null); }
			set { this.GetType().GetProperty(propertyName).SetValue(this, value, null); }
		}

		public void ExtractValue(List<string> itemToExtract) {
			string[] splitItems = itemToExtract[1].Split('=');

			this[splitItems[0]] = splitItems[1];

			splitItems = itemToExtract[2].Split('=');
			this[splitItems[0]] = splitItems[1];
		}
	}
}